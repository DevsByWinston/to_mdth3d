// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "To_MDTH.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, To_MDTH, "To_MDTH" );

DEFINE_LOG_CATEGORY(LogTo_MDTH)
 