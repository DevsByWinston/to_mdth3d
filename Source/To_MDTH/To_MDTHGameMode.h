// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "To_MDTHGameMode.generated.h"

UCLASS(minimalapi)
class ATo_MDTHGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATo_MDTHGameMode();
};



